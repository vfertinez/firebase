package com.example.firebasetemplate;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.FirebaseFirestore;


public class ViewPostFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_view_post, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String postid = ViewPostFragmentArgs.fromBundle(getArguments()).getPostid();

        TextView contenido = view.findViewById(R.id.contenido);
        ImageView imagePost = view.findViewById(R.id.photo);
        ImageView imageAuthor = view.findViewById(R.id.photouser);
        TextView infoPost = view.findViewById(R.id.textPost);

        FirebaseFirestore.getInstance().collection("posts").document(postid).get().addOnSuccessListener(documentSnapshot -> {
            Post post = documentSnapshot.toObject(Post.class);
            contenido.setText(post.authorName);
            Glide.with(requireContext()).load(post.imageUrl).into(imagePost);
            Glide.with(requireContext()).load(post.authorImage).circleCrop().into(imageAuthor);
            infoPost.setText(post.content);

        });

        // firebase.collection("posts").document(postid).get().addOnComplete(
        //
        //  contenido.setText();
        // )



//
    }
}