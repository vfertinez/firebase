package com.example.firebasetemplate;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.firebasetemplate.databinding.FragmentProfileBinding;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.Query;

import java.util.UUID;


public class ProfileFragment extends AppFragment {
    private FragmentProfileBinding binding;
    private Uri uriImagen;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = FragmentProfileBinding.inflate(inflater, container, false)).getRoot();

    }

    Query setQuery() {
        return db.collection("users");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.confirm.setVisibility(View.INVISIBLE);
        Glide.with(this).load(auth.getCurrentUser().getPhotoUrl()).into(binding.previo);

        binding.cambiafoto.setOnClickListener(v -> galeria.launch("image/*"));

        appViewModel.uriImagenSeleccionada.observe(getViewLifecycleOwner(), uri -> {
            Glide.with(this).load(uri).into(binding.previo);
            uriImagen = uri;
            binding.confirm.setVisibility(View.VISIBLE);
        });
        binding.Nombre.setText(auth.getCurrentUser().getDisplayName());
;





        binding.confirm.setOnClickListener(v -> {
            binding.confirm.setEnabled(false);
            storage.getReference("/images/" + UUID.randomUUID() + ".jpg")
                    .putFile(uriImagen)
                    .continueWithTask(task -> task.getResult().getStorage().getDownloadUrl()
                            .addOnSuccessListener(urlDescarga -> {
                                FirebaseUser user = auth.getCurrentUser();

                                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
//                                .setDisplayName("Jane Q. User")
                                        .setPhotoUri(Uri.parse(urlDescarga.toString()))
                                        .build();

                                user.updateProfile(profileUpdates)
                                        .addOnCompleteListener(task1 -> {
                                            if (task1.isSuccessful()) {
                                                // AQUI YA SE HA ACTUALIZADO EL PERFIL CORRECTAMENT
                                                Log.d("AAAA", "User profile updated.");
                                            }
                                        });
                            }));
        });
    }

    private final ActivityResultLauncher<String> galeria = registerForActivityResult(new ActivityResultContracts.GetContent(), uri -> {
        appViewModel.setUriImagenSeleccionada(uri);
    });
}