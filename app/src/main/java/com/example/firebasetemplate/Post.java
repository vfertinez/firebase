package com.example.firebasetemplate;

import java.util.ArrayList;
import java.util.HashMap;

public class Post {
    public String postid;
    public String content;
    public String authorName;
    public String authorImage;
    public String date;
    public String imageUrl;
    public HashMap<String, Boolean> likes = new HashMap<>();
    public ArrayList<String> comments = new ArrayList<>();
}