package com.example.firebasetemplate;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.firebasetemplate.databinding.FragmentRegisterBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import java.util.UUID;


public class RegisterFragment extends AppFragment {
    private FragmentRegisterBinding binding;
    private Uri uriImagen;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = FragmentRegisterBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.previsualizacion2.setOnClickListener(v -> galeria.launch("image/*"));

        appViewModel.uriImagenSeleccionada.observe(getViewLifecycleOwner(), uri -> {
            Glide.with(this).load(uri).into(binding.previsualizacion2);
            uriImagen = uri;
        });

        binding.createAccountButton.setOnClickListener(v -> {
            if (binding.passwordEditText.getText().toString().isEmpty()) {
                binding.passwordEditText.setError("Required");
                return;
            }

            FirebaseAuth.getInstance()
                    .createUserWithEmailAndPassword(
                            binding.emailEditText.getText().toString(),
                            binding.passwordEditText.getText().toString()
                    ).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    navController.navigate(R.id.action_registerFragment_to_postsHomeFragment);
                } else {
                    Toast.makeText(requireContext(), task.getException().getLocalizedMessage(),
                            Toast.LENGTH_SHORT).show();
                }

            });
            storage.getReference("/images/"+ UUID.randomUUID() + ".jpg")
                    .putFile(uriImagen)
                    .continueWithTask(task -> task.getResult().getStorage().getDownloadUrl()
                    .addOnSuccessListener(urlDescarga ->{
                        FirebaseUser user = auth.getCurrentUser();
                        UserProfileChangeRequest profileCompleter = new UserProfileChangeRequest.Builder()
                                .setDisplayName(String.valueOf(binding.Name.getText()))
                                .setPhotoUri(Uri.parse(urlDescarga.toString())).build();
                        user.updateProfile(profileCompleter)
                                .addOnCompleteListener(task1 -> {
                                   if(task1.isSuccessful()){
                                       Log.d("AAA","User Created");
                                   }
                                });

                    }));
        });
    }
    private final ActivityResultLauncher<String> galeria = registerForActivityResult(new ActivityResultContracts.GetContent(), uri -> {
        appViewModel.setUriImagenSeleccionada(uri);
    });

}